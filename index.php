<!DOCTYPE html>
<html lang="ru">
    <head>
        <title>Заголовок</title>
        <meta name="keywords" content="Ключевики"/>
        <meta name="description" content="Описание"/>
        <meta property="og:description" content="Описание"/>
        <meta property="og:title" content="Заголовок"/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content="http://razumaks.ru"/>
        <meta property="og:image" content="http://razumaks/og.png"/>
        <meta property="og:image:secure_url" content="http://razumaks.ru/og.png"/>
        <meta property="og:image:type" content="image/png"/>
        <meta property="og:image:width" content="196"/>
        <meta property="og:image:height" content="196"/>
        <link rel="stylesheet" href="css/style.css"/>
        <link rel="stylesheet" href="css/media_screen.css"/>
        <link rel="apple-touch-icon-precomposed" sizes="57x57" href="apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="apple-touch-icon-144x144.png">
        <link rel="apple-touch-icon-precomposed" sizes="60x60" href="apple-touch-icon-60x60.png">
        <link rel="apple-touch-icon-precomposed" sizes="120x120" href="apple-touch-icon-120x120.png">
        <link rel="apple-touch-icon-precomposed" sizes="76x76" href="apple-touch-icon-76x76.png">
        <link rel="apple-touch-icon-precomposed" sizes="152x152" href="apple-touch-icon-152x152.png">
        <link rel="icon" type="image/png" href="favicon-196x196.png" sizes="196x196">
        <link rel="icon" type="image/png" href="favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="favicon-128.png" sizes="128x128">
        <link rel="icon" href="favicon.ico">
        <!-- подключение библиотеки jquery -->
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
        <!-- определение адаптивного дизайна -->
        <script type="text/javascript">
            var width = screen.width;
            if (width <= '320') {
                document.write('<meta name="viewport" content="width=320">');
            }
            if (width > '320' && width <= '420') {
                document.write('<meta name="viewport" content="width=420">');
            }
            if (width > '420' && width <= '600') {
                document.write('<meta name="viewport" content="width=600">');
            }
            if (width > '600' && width <= '700') {
                document.write('<meta name="viewport" content="width=700">');
            }
            if (width > '700' && width <= '960') {
                document.write('<meta name="viewport" content="width=960">');
            }
            if (width > '960' && width <= '1366') {
                document.write('<meta name="viewport" content="width=1366">');
            }
        </script>
        <meta content='true' name='HandheldFriendly'/>
        <meta content='width' name='MobileOptimized'/>
        <meta content='yes' name='apple-mobile-web-app-capable'/>
        <!--[if lt IE 9]> <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script> <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script> <![endif]-->
        <meta charset="utf-8"/>
    </head>
    <body>
        <div id="main_header">
            <header class="wraper">
                <div id="left_header">
                    <p>opt@liquidlab.ru</p>
                    <p>+7(960) 831 25 53</p>
                </div>
                <div id="center_header">
                    <img src="img/logo.png" alt="">
                    <p>Оптовый онлайн заказ</p>
                </div>
                <div id="right_header">
                    <p id="full_sum2">Сумма заказа:
                        <span class="value">0</span>
                        <span>р.</span>
                    </p>
                    <p class="scroll-bottom">
                        <a href="#order-form">Оформить заказ</a>
                    </p>
                </div>
            </header>
        </div>
        <div id="main_th_poz">
            <table id="th_poz">
                <tr>
                    <th id="th_poz_img">Фото</th>
                    <th id="th_poz_name">Наименование позиции</th>
                    <th id="th_poz_nalichie">Наличие</th>
                    <th id="th_poz_price">Цена</th>
                    <th id="th_poz_input">кол-во</th>
                    <th id="th_poz_sum">Сумма</th>
                </tr>
            </table>
        </div>
        <section>
            <article class="category">
                <h2 class="category_name">Аккумуляторы</h2>
                <div class="pozition_section">
                    <div class="pozition">
                        <p class="poz_nombe">1</p>
                        <p class="poz_img">
                            <a class="fancybox" data-fancybox-group="A892.jpg" href="img/catalog/A892.jpg" title="Комплект Eleaf iStick Power Nano 1100 mAh + Бак MELO III Nano 2ml (Текстура древесины)"><img alt="" src="img/catalog/A892.jpg"></a>
                            <a class="fancybox hidden" data-fancybox-group="A892.jpg" href="img/catalog/A892.jpg" title="Комплект Eleaf iStick Power Nano 1100 mAh + Бак MELO III Nano 2ml (Текстура древесины)"></a>
                            <a class="fancybox hidden" data-fancybox-group="A892.jpg" href="img/catalog/A892.jpg" title="Комплект Eleaf iStick Power Nano 1100 mAh + Бак MELO III Nano 2ml (Текстура древесины)"></a>
                        </p>
                        <p class="poz_youtube">
                            <a class="video" href="https://www.youtube.com/watch?v=HtV5gK3aofU"><img src="img/icon_youtube.png" alt=""></a>
                        </p>
                        <p class="poz_name">Комплект Eleaft iStick Power Nano 1100 mAh + Бак MELO lll Nano 2ml (Текстура древесины)</p>
                        <p class="poz_nalichie">в наличии</p>
                        <p class="poz_price product-price">2190</p>
                        <p class="poz_input"><input form="order-form" type="number"></p>
                        <p class="poz_sum">
                            -
                        </p>
                    </div>
                    <div class="pozition">
                        <p class="poz_nombe">1</p>
                        <p class="poz_img">
                            <a class="fancybox" data-fancybox-group="A892_2.jpg" href="img/catalog/A892.jpg" title="Комплект Eleaf iStick Power Nano 1100 mAh + Бак MELO III Nano 2ml (Текстура древесины)"><img alt="" src="img/catalog/A892.jpg"></a>
                            <a class="fancybox hidden" data-fancybox-group="A892_2.jpg" href="img/catalog/A892.jpg" title="Комплект Eleaf iStick Power Nano 1100 mAh + Бак MELO III Nano 2ml (Текстура древесины)"></a>
                            <a class="fancybox hidden" data-fancybox-group="A892_2.jpg" href="img/catalog/A892.jpg" title="Комплект Eleaf iStick Power Nano 1100 mAh + Бак MELO III Nano 2ml (Текстура древесины)"></a>
                        </p>
                        <p class="poz_youtube">
                            <a class="video" href="https://www.youtube.com/watch?v=HtV5gK3aofU"><img src="img/icon_youtube.png" alt=""></a>
                        </p>
                        <p class="poz_name">Комплект Eleaft iStick Power Nano 1100 mAh + Бак MELO lll Nano 2ml (Текстура древесины)</p>
                        <p class="poz_nalichie">в наличии</p>
                        <p class="poz_price">2190</p>
                        <p class="poz_input"><input form="order-form" type="number"></p>
                        <p class="poz_sum">
                            -
                        </p>
                    </div>
                    <div class="pozition">
                        <p class="poz_nombe">1</p>
                        <p class="poz_img">
                            <a class="fancybox" data-fancybox-group="A892_3.jpg" href="img/catalog/A892.jpg" title="Комплект Eleaf iStick Power Nano 1100 mAh + Бак MELO III Nano 2ml (Текстура древесины)"><img alt="" src="img/catalog/A892.jpg"></a>
                            <a class="fancybox hidden" data-fancybox-group="A892_3.jpg" href="img/catalog/A892.jpg" title="Комплект Eleaf iStick Power Nano 1100 mAh + Бак MELO III Nano 2ml (Текстура древесины)"></a>
                            <a class="fancybox hidden" data-fancybox-group="A892_3.jpg" href="img/catalog/A892.jpg" title="Комплект Eleaf iStick Power Nano 1100 mAh + Бак MELO III Nano 2ml (Текстура древесины)"></a>
                        </p>
                        <p class="poz_youtube">
                            <a class="video" href="https://www.youtube.com/watch?v=HtV5gK3aofU"><img src="img/icon_youtube.png" alt=""></a>
                        </p>
                        <p class="poz_name">Комплект Eleaft iStick Power Nano 1100 mAh + Бак MELO lll Nano 2ml (Текстура древесины)</p>
                        <p class="poz_nalichie">в наличии</p>
                        <p class="poz_price">2190</p>
                        <p class="poz_input"><input form="order-form" type="number"></p>
                        <p class="poz_sum">
                            -
                        </p>
                    </div>
                    <div class="pozition">
                        <p class="poz_nombe">1</p>
                        <p class="poz_img">
                            <a class="fancybox" data-fancybox-group="A892_4.jpg" href="img/catalog/A892.jpg" title="Комплект Eleaf iStick Power Nano 1100 mAh + Бак MELO III Nano 2ml (Текстура древесины)"><img alt="" src="img/catalog/A892.jpg"></a>
                            <a class="fancybox hidden" data-fancybox-group="A892_4.jpg" href="img/catalog/A892.jpg" title="Комплект Eleaf iStick Power Nano 1100 mAh + Бак MELO III Nano 2ml (Текстура древесины)"></a>
                            <a class="fancybox hidden" data-fancybox-group="A892_4.jpg" href="img/catalog/A892.jpg" title="Комплект Eleaf iStick Power Nano 1100 mAh + Бак MELO III Nano 2ml (Текстура древесины)"></a>
                        </p>
                        <p class="poz_youtube">
                            <a class="video" href="https://www.youtube.com/watch?v=HtV5gK3aofU"><img src="img/icon_youtube.png" alt=""></a>
                        </p>
                        <p class="poz_name">Комплект Eleaft iStick Power Nano 1100 mAh + Бак MELO lll Nano 2ml (Текстура древесины)</p>
                        <p class="poz_nalichie">в наличии</p>
                        <p class="poz_price">2190</p>
                        <p class="poz_input"><input form="order-form" type="number"></p>
                        <p class="poz_sum">
                            -
                        </p>
                    </div>
                    <div class="pozition">
                        <p class="poz_nombe">1</p>
                        <p class="poz_img">
                            <a class="fancybox" data-fancybox-group="A892_5.jpg" href="img/catalog/A892.jpg" title="Комплект Eleaf iStick Power Nano 1100 mAh + Бак MELO III Nano 2ml (Текстура древесины)"><img alt="" src="img/catalog/A892.jpg"></a>
                            <a class="fancybox hidden" data-fancybox-group="A892_5.jpg" href="img/catalog/A892.jpg" title="Комплект Eleaf iStick Power Nano 1100 mAh + Бак MELO III Nano 2ml (Текстура древесины)"></a>
                            <a class="fancybox hidden" data-fancybox-group="A892_5.jpg" href="img/catalog/A892.jpg" title="Комплект Eleaf iStick Power Nano 1100 mAh + Бак MELO III Nano 2ml (Текстура древесины)"></a>
                        </p>
                        <p class="poz_youtube">
                            <a class="video" href="https://www.youtube.com/watch?v=HtV5gK3aofU"><img src="img/icon_youtube.png" alt=""></a>
                        </p>
                        <p class="poz_name">Комплект Eleaft iStick Power Nano 1100 mAh + Бак MELO lll Nano 2ml (Текстура древесины)</p>
                        <p class="poz_nalichie">в наличии</p>
                        <p class="poz_price">2190</p>
                        <p class="poz_input"><input type="number" min="0"></p>
                        <p class="poz_sum">
                            -
                        </p>
                    </div>
                </div>
            </article>
            <article class="category">
                <h2 class="category_name">Аккумуляторы</h2>
                <div class="pozition_section">
                    <div class="pozition">
                        <p class="poz_nombe">1</p>
                        <p class="poz_img">
                            <a class="fancybox" data-fancybox-group="A892_6.jpg" href="img/catalog/A892.jpg" title="Комплект Eleaf iStick Power Nano 1100 mAh + Бак MELO III Nano 2ml (Текстура древесины)"><img alt="" src="img/catalog/A892.jpg"></a>
                            <a class="fancybox hidden" data-fancybox-group="A892_6.jpg" href="img/catalog/A892.jpg" title="Комплект Eleaf iStick Power Nano 1100 mAh + Бак MELO III Nano 2ml (Текстура древесины)"></a>
                            <a class="fancybox hidden" data-fancybox-group="A892_6.jpg" href="img/catalog/A892.jpg" title="Комплект Eleaf iStick Power Nano 1100 mAh + Бак MELO III Nano 2ml (Текстура древесины)"></a>
                        </p>
                        <p class="poz_youtube">
                            <a class="video" href="https://www.youtube.com/watch?v=HtV5gK3aofU"><img src="img/icon_youtube.png" alt=""></a>
                        </p>
                        <p class="poz_name">Комплект Eleaft iStick Power Nano 1100 mAh + Бак MELO lll Nano 2ml (Текстура древесины)</p>
                        <p class="poz_nalichie">в наличии</p>
                        <p class="poz_price">9190</p>
                        <p class="poz_input"><input type="number" min="0"></p>
                        <p class="poz_sum">
                            -
                        </p>
                    </div>
                    <div class="pozition">
                        <p class="poz_nombe">1</p>
                        <p class="poz_img">
                            <a class="fancybox" data-fancybox-group="A892_7.jpg" href="img/catalog/A892.jpg" title="Комплект Eleaf iStick Power Nano 1100 mAh + Бак MELO III Nano 2ml (Текстура древесины)"><img alt="" src="img/catalog/A892.jpg"></a>
                            <a class="fancybox hidden" data-fancybox-group="A892_7.jpg" href="img/catalog/A892.jpg" title="Комплект Eleaf iStick Power Nano 1100 mAh + Бак MELO III Nano 2ml (Текстура древесины)"></a>
                            <a class="fancybox hidden" data-fancybox-group="A892_7.jpg" href="img/catalog/A892.jpg" title="Комплект Eleaf iStick Power Nano 1100 mAh + Бак MELO III Nano 2ml (Текстура древесины)"></a>
                        </p>
                        <p class="poz_youtube">
                            <a class="video" href="https://www.youtube.com/watch?v=HtV5gK3aofU"><img src="img/icon_youtube.png" alt=""></a>
                        </p>
                        <p class="poz_name">Комплект Eleaft iStick Power Nano 1100 mAh + Бак MELO lll Nano 2ml (Текстура древесины)</p>
                        <p class="poz_nalichie">в наличии</p>
                        <p class="poz_price">2190</p>
                        <p class="poz_input"><input type="number" min="0"></p>
                        <p class="poz_sum">
                            -
                        </p>
                    </div>
                    <div class="pozition">
                        <p class="poz_nombe">1</p>
                        <p class="poz_img">
                            <a class="fancybox" data-fancybox-group="A892_8.jpg" href="img/catalog/A892.jpg" title="Комплект Eleaf iStick Power Nano 1100 mAh + Бак MELO III Nano 2ml (Текстура древесины)"><img alt="" src="img/catalog/A892.jpg"></a>
                            <a class="fancybox hidden" data-fancybox-group="A892_8.jpg" href="img/catalog/A892.jpg" title="Комплект Eleaf iStick Power Nano 1100 mAh + Бак MELO III Nano 2ml (Текстура древесины)"></a>
                            <a class="fancybox hidden" data-fancybox-group="A892_8.jpg" href="img/catalog/A892.jpg" title="Комплект Eleaf iStick Power Nano 1100 mAh + Бак MELO III Nano 2ml (Текстура древесины)"></a>
                        </p>
                        <p class="poz_youtube">
                            <a class="video" href="https://www.youtube.com/watch?v=HtV5gK3aofU"><img src="img/icon_youtube.png" alt=""></a>
                        </p>
                        <p class="poz_name">Комплект Eleaft iStick Power Nano 1100 mAh + Бак MELO lll Nano 2ml (Текстура древесины)</p>
                        <p class="poz_nalichie">в наличии</p>
                        <p class="poz_price">2190</p>
                        <p class="poz_input"><input type="number" min="0"></p>
                        <p class="poz_sum">
                            -
                        </p>
                    </div>
                    <div class="pozition">
                        <p class="poz_nombe">1</p>
                        <p class="poz_img">
                            <a class="fancybox" data-fancybox-group="A892_9.jpg" href="img/catalog/A892.jpg" title="Комплект Eleaf iStick Power Nano 1100 mAh + Бак MELO III Nano 2ml (Текстура древесины)"><img alt="" src="img/catalog/A892.jpg"></a>
                            <a class="fancybox hidden" data-fancybox-group="A892_9.jpg" href="img/catalog/A892.jpg" title="Комплект Eleaf iStick Power Nano 1100 mAh + Бак MELO III Nano 2ml (Текстура древесины)"></a>
                            <a class="fancybox hidden" data-fancybox-group="A892_9.jpg" href="img/catalog/A892.jpg" title="Комплект Eleaf iStick Power Nano 1100 mAh + Бак MELO III Nano 2ml (Текстура древесины)"></a>
                        </p>
                        <p class="poz_youtube">
                            <a class="video" href="https://www.youtube.com/watch?v=HtV5gK3aofU"><img src="img/icon_youtube.png" alt=""></a>
                        </p>
                        <p class="poz_name">Комплект Eleaft iStick Power Nano 1100 mAh + Бак MELO lll Nano 2ml (Текстура древесины)</p>
                        <p class="poz_nalichie">в наличии</p>
                        <p class="poz_price">2190</p>
                        <p class="poz_input"><input type="number" min="0"></p>
                        <p class="poz_sum">
                            -
                        </p>
                    </div>
                    <div class="pozition">
                        <p class="poz_nombe">1</p>
                        <p class="poz_img">
                            <a class="fancybox" data-fancybox-group="A892_10.jpg" href="img/catalog/A892.jpg" title="Комплект Eleaf iStick Power Nano 1100 mAh + Бак MELO III Nano 2ml (Текстура древесины)"><img alt="" src="img/catalog/A892.jpg"></a>
                            <a class="fancybox hidden" data-fancybox-group="A892_10.jpg" href="img/catalog/A892.jpg" title="Комплект Eleaf iStick Power Nano 1100 mAh + Бак MELO III Nano 2ml (Текстура древесины)"></a>
                            <a class="fancybox hidden" data-fancybox-group="A892_10.jpg" href="img/catalog/A892.jpg" title="Комплект Eleaf iStick Power Nano 1100 mAh + Бак MELO III Nano 2ml (Текстура древесины)"></a>
                        </p>
                        <p class="poz_youtube">
                            <a class="video" href="https://www.youtube.com/watch?v=HtV5gK3aofU"><img src="img/icon_youtube.png" alt=""></a>
                        </p>
                        <p class="poz_name">Комплект Eleaft iStick Power Nano 1100 mAh + Бак MELO lll Nano 2ml (Текстура древесины)</p>
                        <p class="poz_nalichie">в наличии</p>
                        <p class="poz_price">2190</p>
                        <p class="poz_input"><input type="number" min="0"></p>
                        <p class="poz_sum">
                            -
                        </p>
                    </div>
                </div>
            </article>
        </section>
        <div id="main_footer">
            <footer>
                <p id="full_sum">Сумма заказа:
                    <span class="value">0</span>
                    <span>р.</span>
                </p>
                <p id="footerdesc">Заполните контактную информацию и отправьте заявку</p>
                <form id="form_zakaz" name="form_zakaz" action="" method="post" onsubmit="return false;">
                    <input type="text" hidden="" name="total-list">
                    <input type="text" hidden="" name="total-price">
                    <p>
                        <label for="">Название организации</label><br>
                        <input type="text" name="company" placeholder="ИП Иванов И.И.">
                    </p>
                    <p>
                        <label for="">Ф.И.О.*</label><br>
                        <input type="text" name="fio" required="" placeholder="Иванов Иван Иванович">
                    </p>
                    <p>
                        <label for="">Город*</label><br>
                        <input type="text" name="city" required="" placeholder="Ваш город получения">
                    </p>
                    <p>
                        <label for="">E-mail*</label><br>
                        <input type="email" name="email" required="" placeholder="Ваш e-mail">
                    </p>
                    <p>
                        <label for="">Телефон*</label><br>
                        <input type="tel" name="phone" required="" placeholder="Номер телефона для связи">
                    </p>
                    <p>
                        <label for="">Коментарий к заказу</label><br>
                        <textarea name="comments" cols="30" rows="10"></textarea>
                    </p>
                    <input type="submit" name="send_post" id="createOrderButton" value="отправить заказ">
                </form>
            </footer>
        </div>

        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.fancybox.pack.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>
