var hMainHeader;

$(document).ready(function() {
    $('body').on('click', '.pozition', function() {
        if ($(this).hasClass('selected')) {
            $(this).not('.disabled-row').removeClass('selected');
        } else {
            $('.pozition').not('.disabled-row').removeClass('selected');
            $(this).not('.disabled-row').addClass('selected');
        }
    });

    var $root = $('html, body');
    $('.scroll-bottom a').click(function() {
        $root.animate({
            'scrollTop': $("#form_zakaz").offset().top - $("header").outerHeight()
        }, 500);
        return false;
    });

    if ($('.fancybox').length) {
        $('.fancybox').fancybox({
            loop: true
        });
    }

    if ($(".video").length) {
        $(".video").click(function() {
            $.fancybox({
                'padding': 0,
                'autoScale': false,
                'transitionIn': 'none',
                'transitionOut': 'none',
                'title': this.title,
                'width': 640,
                'height': 385,
                'href': this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
                'type': 'swf',
                'swf': {
                    'wmode': 'transparent',
                    'allowfullscreen': 'true'
                },
                youtube: {
                    autoplay: 1, // enable autoplay
                    //start: 47 // set start time in seconds (embed)
                }
            });

            return false;
        });
    }

    $(".poz_input input").bind("change keyup", function() {
        var opt = 50000;
        var calc_rozn = calc();
        console.log(calc_rozn);

        if (calc_rozn > 0) {
            $("#full_sum .value, #full_sum2 .value").text(calc_rozn);
        } else {
            $("#full_sum .value, #full_sum2 .value").text("0");
        }


        $("[name='total-price']").val(calc_rozn);
    });

    function calc() {
        var summ = 0;
        $(".poz_input input").each(function() {
            if ($(this).val() !== '') {
                var col = parseInt($(this).val());
                var price = parseInt($(this).parents('.pozition').find('.poz_price').text());
                var finalprice = col * price;
                $(this).parents('.pozition').find('.poz_sum').text(finalprice);
                summ += finalprice;
            } else {
              $(this).parents('.pozition').find('.poz_sum').text("-");
            }
        });
        return summ;
    }

    function clear_inp() {
        $(".poz_input input").each(function() {
            if ($(this).val() !== '') {
                $(this).val("");
            }
        });
        $("#full_sum .value, #full_sum2 .value").text("0");
    }

    function getPostData() {
        var products = [];
        $(".poz_input input").each(function() {
            if ($(this).val() !== '') {
                var arr = [];
                var col = parseInt($(this).val());
                var price = parseInt($(this).parents('.pozition').find('.poz_price').text());
                var poz_name = $(this).parents('.pozition').find('.poz_name').text();
                var finalprice = col * price;
                arr = [poz_name, price, col, finalprice];
                products.push(arr);
            }
        });

        return products;
    }


    $("#createOrderButton").on('click', function(event) {
        // event.preventDefault();
        // console.log(getPostData());
        $("[name='total-list']").val(JSON.stringify(getPostData()));
        if ($("#form_zakaz input[type=tel]").val() !== "" && $("#form_zakaz input[type=email]").val() !== "") {
            $.post("ordering.php", $("#form_zakaz").serialize() /*+ '&' + $("input.amount-input").serialize()*/ , function(data) {
                console.log(data);
                if (data.type == 'ok') {
                    var text = data.order;
                    $("#form_zakaz input[type=text], #form_zakaz input[type=tel], #form_zakaz input[type=email], #form_zakaz textarea, input.amount-input").val("");
                    clear_inp();
                    calc();
                } else {
                    var text = "";
                };
                $.fancybox({
                    'content': "<h3 style='text-align:center'>" + text + "</h3>"
                });
            }, "json").fail(function() {
                console.log(arguments);
            });
        }
    });
});
