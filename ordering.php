﻿<?php
if (isset ($_POST['phone'])) {/*Проверяем условие нажатия кнопки отправки*/
    // формируем переменные, которые содержат данные, полученные с html-формы
    $name = $_POST['fio'];
    $tel = $_POST['phone'];

    $company = $_POST['company'];
    $city = $_POST['city'];
    $email = $_POST['email'];
    $comments = $_POST['comments'];

    $totalprice = $_POST['total-price'];

    $to = "hydraconis@yandex.ru"; // почта для приема заявок
    $tema = "Заявка с сайта! " . $_SERVER['SERVER_NAME'];
    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
    $headers .= "From: < hydraconis@yandex.ru > \r\n";
    //$headers .= "Reply-To: zakaz@".$_SERVER['SERVER_NAME']."\r\n";

    // Фильтруем введённые данные от спец. символов, удаляем случайные пробелы из полей ввода, фрмируя значения переменных
    if (isset ($name)) {
        htmlspecialchars($name);
        trim($name);
    }
    if ($name == "") {
        unset ($name);
    }
    if (isset ($tel)) {
        htmlspecialchars($tel);
        trim($tel);
    }
    if ($tel == "") {
        unset ($tel);
    }

    if (isset ($_POST['total-list'])) {
        $message_table .= "<table border=1 width=100%>";
        $message_table .= "<tr>";
        $message_table .= "<th>" . "Наименование" . "</th>";
        $message_table .= "<th>" . "Цена" . "</th>";
        $message_table .= "<th>" . "Количество" . "</th>";
        $message_table .= "<th>" . "Сумма" . "</th>";
        $message_table .= "</tr>";
        foreach (json_decode($_POST["total-list"], true) as $key => $value_prod) {
            $message_table .= "<tr>";
            $message_table .= "<td>" . $value_prod[0] . "</td>";
            $message_table .= "<td>" . $value_prod[1] . "</td>";
            $message_table .= "<td>" . $value_prod[2] . "</td>";
            $message_table .= "<td>" . $value_prod[3] . "</td>";
            $message_table .= "</tr>";
            // var_dump($value_prod);
        }
        $message_table .= "</table>";
    }

    /*Проверяем заполнение полей (наличие переменных)*/
    if (isset ($name) and isset ($tel)) {
        $message .= "Имя отправителя: " . $name . "\n<br>Телефон: " . $tel . "\n<br>";
        $message .= "Организация: " . $company . "\n<br>Город: " . $city . "\n<br>";
        $message .= "Email: " . $email . "\n<br>Комментарии:<br> " . $comments . "\n<br><br>";

        $message .= "Состав заказа: \n<br>";
        $message .= $message_table;
        $message .= "Сумма заказа: " . $totalprice . "\n<br>";

        $result = mail($to, $tema, $message, $headers);
        if ($result == 'true') {
            echo json_encode(array('type' => "ok", 'order' => "Ваше сообщение отправлено"));
        }
    }
}
?>